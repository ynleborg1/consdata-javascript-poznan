
export function Products(products = []) {
  this.products = products
  this._index = {}
  this._buildIndex()
}
Products.prototype = {

  _buildIndex: function () {
    for (var i in this.products) {
      var product = this.products[i]
      this._index[product.id] = product
    }
  },

  getById: function (id) {
    return this._index[id]
  },

  setData: function (data) {
    this.products = data
    this._buildIndex()
  }
}
