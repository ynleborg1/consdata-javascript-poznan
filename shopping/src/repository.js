export function AbstractRepository() {

  this.url = ''
}
AbstractRepository.prototype = {
  fetchAll: function () {
    return fetch(this.url)
      .then(function (response) {
        return response.json()
      })
  }
}


export function ProductsRepository() {
  AbstractRepository.apply(this,arguments)
  this.url = 'http://localhost:8080/data/products.json'
}
ProductsRepository.prototype = Object.create(AbstractRepository.prototype)



export function ItemsRepository() {
  AbstractRepository.apply(this,arguments)
  this.url = 'http://localhost:8080/data/items.json'
}
ItemsRepository.prototype = Object.create(AbstractRepository.prototype)